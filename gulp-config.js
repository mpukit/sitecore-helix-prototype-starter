module.exports = function () {
    // Default Destination
    //var instanceRoot = "C:\\inetpub\\wwwroot\\asahq.local";
    // For Prototype Build
    var instanceRoot = "./src/Project/MainSite/code";

    var config = {
        websiteRoot: instanceRoot,
        sitecoreLibraries: instanceRoot + "\\bin",
        licensePath: instanceRoot + "\\App_Data\\license.xml",
        solutionName: "ASA - Website",
        buildConfiguration: "Debug",
        buildToolsVersion: 15.0,
        buildMaxCpuCount: 0,
        buildVerbosity: "minimal",
        buildPlatform: "Any CPU",
        publishPlatform: "AnyCpu",
        runCleanBuilds: false
    };
    return config;
}