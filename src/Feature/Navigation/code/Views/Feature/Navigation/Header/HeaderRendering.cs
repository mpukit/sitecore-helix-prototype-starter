﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace ASA.Website.Feature.Navigation.Views.Feature.Navigation.Header
{
    public class HeaderRendering
    {
        [SitecoreQuery("./*[@@templateid='{CB5E34D1-8390-4A93-8EA5-C5ED4E3F4A2A}']", IsLazy = false, IsRelative = true)]
        public virtual IEnumerable<NavigationSection> NavigationSections { get; set; }

        [SitecoreQuery("./*[@@templateid='{676021AA-FDB7-4373-80EB-E92984734715}']", IsLazy = false, IsRelative = true)]
        public virtual IEnumerable<DirectLink> DirectLinks { get; set; }
    }

    public class NavigationSection
    {
        [SitecoreField(Templates._Navigable.Fields.Heading)]
        public virtual string Heading { get; set; }

        [SitecoreChildren(IsLazy = false)]
        public virtual IEnumerable<DirectLink> DirectLinks { get; set; }
    }

    public class DirectLink
    {
        [SitecoreField(Templates._Link.Fields.Text)]
        public virtual string Text { get; set; }

        [SitecoreField(Templates._Link.Fields.Link)]
        public virtual Link Link { get; set; }
    }
}