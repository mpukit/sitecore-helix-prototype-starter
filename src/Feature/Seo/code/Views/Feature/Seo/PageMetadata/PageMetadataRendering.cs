﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace ASA.Website.Feature.Seo.Views.Feature.Seo.PageMetadata
{
    public class PageMetadataRendering
    {
        [SitecoreField(Templates._PageMetadata.Fields.Browser_Title)]
        public virtual string BrowserTitle { get; set; }

        [SitecoreField(Templates._PageMetadata.Fields.Description)]
        public virtual string Description { get; set; }

        [SitecoreField(Templates._PageMetadata.Fields.Keywords)]
        public virtual string Keywords { get; set; }

        [SitecoreField(Templates._PageMetadata.Fields.Robots)]
        public virtual string Robots { get; set; }
    }
}