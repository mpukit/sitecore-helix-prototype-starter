
﻿





#pragma warning disable 1591
#pragma warning disable 0108
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Team Development for Sitecore.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;   
using System.Collections.Generic;   
using System.Linq;
using System.Text;
﻿
﻿
﻿


namespace ASA.Website.Feature.Seo.Templates
{
 	/// <summary>
	/// _PageMetadata
	/// <para></para>
	/// <para>Path: /sitecore/templates/Feature/Seo/_PageMetadata</para>	
	/// <para>ID: 0d62c7e5-140f-460d-8b69-f3eecb03b1d8</para>	
	/// </summary>
	public static class _PageMetadata
	{
		public const string Id = "{0d62c7e5-140f-460d-8b69-f3eecb03b1d8}";

		public static class Fields
		{
		
			/// <summary>
			/// The Browser Title field.
			/// <para></para>
			/// <para>Field Type: Single-Line Text</para>		
			/// <para>Field ID: 3effdf83-b324-43f8-8586-47385ea7de08</para>
			/// <para>Custom Data: </para>
			/// </summary>
			public const string Browser_Title = "Browser Title";
		
			/// <summary>
			/// The Description field.
			/// <para></para>
			/// <para>Field Type: Multi-Line Text</para>		
			/// <para>Field ID: 47904694-e83b-47b9-a46b-3df0d6371d27</para>
			/// <para>Custom Data: </para>
			/// </summary>
			public const string Description = "Description";
		
			/// <summary>
			/// The Keywords field.
			/// <para></para>
			/// <para>Field Type: Multi-Line Text</para>		
			/// <para>Field ID: 32949239-1e11-4b94-9481-bae897220021</para>
			/// <para>Custom Data: </para>
			/// </summary>
			public const string Keywords = "Keywords";
		
			/// <summary>
			/// The Robots field.
			/// <para></para>
			/// <para>Field Type: Single-Line Text</para>		
			/// <para>Field ID: a7fb7069-7a4e-403f-b76c-7ec66945f634</para>
			/// <para>Custom Data: </para>
			/// </summary>
			public const string Robots = "Robots";
		
		}
	}
}
﻿
﻿
﻿
﻿
﻿
﻿
﻿
﻿
﻿
﻿


namespace ASA.Website.Feature.Seo.Renderings
{
	public static class Page_Metadata
	{
		public const string Id = "{a8daae22-c0bf-4f1b-a436-807bc65f1309}";
	}
}
﻿
﻿
