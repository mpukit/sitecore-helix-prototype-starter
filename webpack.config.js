var path = require('path');

module.exports = {
    entry: './src/Project/MainSite/code/Scripts/main.js',
    devtool: 'inline-source-map',
    // externals: {
    //     jquery: 'jQuery'
    // },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    // Add the JSHint loader
    module: {
        rules: [{
            test: /\.js$/, // Run the loader on all .js files
            exclude: /node_modules/, // ignore all files in the node_modules folder
            use: 'jshint-loader'
        }]
    }
};
