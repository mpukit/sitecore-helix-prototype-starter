# Project Information
* Sitecore 9.0 Update 1 (rev. 171219)
* .NET Framework Standard 4.7
* ASP.NET MVC 5.2.3

# Prerequisites

**Software:**

You will need the following software versions installed to support Sitecore. Some dependencies that 
require additional steps or more complex configuration (i.e. Solr) are noted further within this section.

* Visual Studio 2017
* [SQL Server Express 2016 SP1](https://www.microsoft.com/en-us/download/details.aspx?id=54284)
* [Java JRE 1.8.0 151+](https://java.com/en/download/)
* Solr 6.6.2
* IIS 8.5 or 10
* [Web Platform Installer 5.0](https://www.microsoft.com/web/downloads/platform.aspx)
  * Web Deploy 3.6 for Hosting Servers
  * URL Rewrite 2.1
* [Microsoft SQL Sys CLR Types](http://go.microsoft.com/fwlink/?LinkID=849415&clcid=0x409)
  * x86
  * x64
* [Microsoft SQL Server Data-Tier Application Framework (DacFx) version 2016](https://www.microsoft.com/en-us/download/details.aspx?id=53013)
  * x86
  * x64
* Git
* [NodeJS (NPM, Gulp)](https://nodejs.org/en/download/)
* [NuGet CLI](https://www.nuget.org/downloads)
* Sitecore Installation Framework

**Visual Studio Add-Ons:**
* Team Development for Sitecore (TDS)
* Sitecore Rocks
* MarkDown Editor

**NuGet Repositories:**
* [Sitecore](https://sitecore.myget.org/F/sc-packages/api/v3/index.json)
* [Sitecore PowerShell](https://doc.sitecore.net/sitecore_experience_platform/developing/developing_with_sitecore/sitecore_powershell_public_nuget_feed_faq)

## Sitecore Installation Framework
1. Open PowerShell
2. Register the Sitecore PowerShell NuGet repository

        Register-PSRepository -Name SitecoreGallery -SourceLocation https://sitecore.myget.org/F/sc-powershell/api/v2

3. Set the policy

        Set-PSRepository -Name SitecoreGallery -InstallationPolicy Trusted

4. Install the module

        Install-Module -Name SitecoreInstallFramework -Repository SitecoreGallery

5. If the module is already installed, update it instead

        Update-Module SitecoreInstallFramework

## Solr

1. From the *.\install\solr6.6.2* folder, open the **Install-Solr.ps1** file for editing. 
2. Update *$JREPath* and *$JREVersion* variables, based on the exact version or 32-bit vs. 64-bit version of the Java JRE.
3. Start PowerShell in elevated mode and execute the script.
 
        .\Install-Solr.ps1

## NodeJS (NPM, Gulp)
1. Install NodeJS + NPM
2. Open command prompt within the working directory.
3. Install Gulp globally

        npm install gulp -g

4. Restore all packages 

        npm install

5. Install other packages as needed

        npm install {package_name_here} --save-dev 

# Environment Setup
Development is done using a local DB instance and a local Sitecore CMS installation. On publish, 
files are copied over to the CMS installation. On source control gets or check-ins, Sitecore 
items should be synchronized.

## Preparation

**Hosts file entries**

Open your **hosts** file, using an elevated editor, from *C:\Windows\System 32\drivers\etc* and add the following text:

    127.0.0.1 solr
    127.0.0.1 asahq.xconnect
    127.0.0.1 asahq.local
    127.0.0.1 asahq-master.local

**SQL**

1. Open SQL Server Management Studio and execute the following script:

        sp_configure 'contained database authentication', 1; 
        GO 
        RECONFIGURE
        GO

2. Create a new login name **sitecore** and assign role of Sys Admin.  

## Sitecore Install (via SIF-Less)

1. From the *.\Install* folder, start **SIF-less.exe**.
2. On the EZ Mode tab, fill in the following values, as well as those that are specific to your environment (SQL, etc).
   * **License XML:** \{local-solution-path\}\install\Sitecore9.0.1\license.xml
   * **Config Folder:** \{local-solution-path\}\install\Sitecore9.0.1
   * **Sitecore Package:** \{local-solution-path\}\install\Sitecore9.0.1\Sitecore 9.0.1 rev. 171219 (OnPrem)_single.scwdp.zip
   * **xConnect Package:** \{local-solution-path\}\install\Sitecore9.0.1\Sitecore 9.0.1 rev. 171219 (OnPrem)_xp0xconnect.scwdp.zip
   * **Install prefix:** asahq
   * **Site Name:** asahq.local
   * **xConnect Name:** asahq.xconnect
   * **Solr URL:** https://solr:8983/solr
   * **Solr Folder:** C:\solr\solr-6.6.2
   * **Solr Service:** solr-6.6.2
   * **SQL Login:** sitecore 
3. Click 'Generate Files' to create scripts.
4. Start PowerShell in elevated mode, browse to *.\Install\SIFLess* and execute script.

        .\SIFless-EZ-{timestamp}.ps1

## IIS Adjustments
1. Run iisreset /stop.
2. Add additional binding of asahq-master.local to asahq.local site.
3. Make sure the install is under C:\inetpub\wwwroot and update the IIS site settings.
4. Run iisreset /start

## Solution Adjustments
1. Configure local Gulp settings by duplicating gulp-config.js, renaming to gulp-config.js.user, and updating settings if need be.

## Deploying the Solution
* **To deploy the entire solution** with all modules, run the default gulp task from your Task Runner Explorer.
* **To deploy an in-progress feature**, right click the feature's web project and publish.

## Modules/Packages
Sitecore uses a packaging system to install custom modules or migrate content between 
various environments.

*(list packages here)*

## Coveo Install
*(add instructions here)*

# Prototype Setup
1. Ensure all dependencies are installed
        `npm install` on root directory (and `npm install -g plop` for Plop Generator, more below...)
2. Spin up local Prototype Environment
        Task: `gulp serve`

# Creating Modules
To create new modules, use the [plop generator](https://github.com/amwmedia/plop).
1. Open a command prompt at the solution root.
2. Run `plop module` and provide the layer and name of your module.
3. Add a solution folder for your module.
4. Use "add existing project" to add the web and TDS projects created by plop to the solution.
5. If your module will have renderings or templates:
   1. Create the appropriate module folders in the Sitecore content tree. (e.g. /Templates/Feature/ModuleName/ and /Layouts/Renderings/Feature/ModuleName/)
   2. Use TDS's "get items" command to pull in those folders
   3. Open up your TDS project "deployment property manager", and set those folders to "synchronize all children".

## Creating Renderings
1. Open a command prompt at the solution root.
2. run `plop rendering` and provide the layer, module, and name of your rendering.
3. Include the created files under /Views/Feature/Module/RenderingName/ in your project and delete what you don't need.
4. You'll still need to create the rendering definition item is Sitecore and point it to your rendering.

# Server Deployment
*(add instructions here)*

